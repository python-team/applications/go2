Release 0.20121210
------------------

* Bug fixed when print new created directories (--cd mode)

Release 1.20121112
------------------

* Trivial missing directory in --cd mode fixed

Release 1.20120203
------------------

* auto-escape pattern when it is not a valid re.

Release 1.20120111
------------------

* refactoring: new classes UserInputHandler and UserChoiceHandler
* highlight and abbreviate path in the chdir-wizzard

Release 1.20111223
------------------

* removed --cwd
* new --path command line argument

Release 1.20111223
------------------

* new --list-only command line argument
* new --cwd command line argument

Release 1.20111219
------------------

* OSFS fake to reduce initialization time

Release 1.20111216
------------------

* "ENTER without matches bug" fixed

Release 1.20111212
------------------

* Added support for .go2/ignore file

Release 1.20111028
------------------

* Refactoring: QueueReactor, PathBuffer, Menu

Release 1.20111011
------------------

* single match fixed.

Release 1.20111009
------------------

* Subprocess termination fixed.
* Automatic election for single match.

Release 1.20110916
------------------

* New multiprocess version (without GUI)
* Log to /tmp/go2-$USER

Release 0.20110716
------------------

* /usr/bin/go2 assures correct setup.
* Bug with non-existent directories FIXED.


.. Local Variables:
..  coding: utf-8
..  mode: rst
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
